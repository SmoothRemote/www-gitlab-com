---
layout: job_family_page
title: "Social Marketing Manager"
description: "Requirements for Social Media roles at GitLab"
twitter_image: ""
twitter_image_alt: ""
twitter_site: "gitlab"
twitter_creator: "gitlab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Social Marketing Jobs at GitLab

Social Media requires a balance of work behind each of the two words that make up the practice. _Social_, by building trust with our audiences and connecting as personally as appropriate, and _Media_, by scaling our storytelling efforts to brand-level corporate marketing initiatives.

#### The "Social" in Social Media
{:.no_toc}
The world's best brands feel more like a trusted source of information to their audience than a company. They could even develop friendly relationships with their followers. This is because the world's best brands understand people and weave that understanding into everything they do. Social Marketing at GitLab is a key part of building GitLab's brand into one that is loved by its audience of developers, remote work pros, and IT folks up and down the ladder.

Social Marketing at GitLab focuses on understanding people who make up the GitLab community, creating and sharing content over social channels that they'll find valuable, and educating team members in marketing and beyond on social media best practices for brands, personalities, and how we measure success.

#### The "Media" in Social Media
{:.no_toc}
The world's most renowned brands are perceived to be larger than life. As GitLab scales to new heights, the Social Marketing team prioritizes opportunities for brand-level storytelling initiatives. By sharing our company values, perspectives on remote work, or leaning into our product's features, we'll help users understand how they feel about GitLab overall.

#### Our perspective on the practice of Social Media
{:.no_toc}
GitLab is a unique company to work for. While the demands of social marketing don't necessarily turn off, our team has leaned into GitLab's values as a way to build the social marketing department of the future. We do best-in-class work and deliver on our expectations but do not allow the perils of working in social media to burn us out or damage our mental health. We take our company values seriously and integrate them into our daily work as often as possible.

## Responsibilities
_Vary by job grade but include:_

- Design and execute our global social strategies
- Manage risks to the GitLab brand and navigate crises 
- Day-to-day engagement with the community
- Develop and publish unique social content
- Support integrated marketing campaigns, events, and other initiatives
- Report on social performance, including insights and forecasts
- Conduct research from listening and other sources to provide quantitative and qualitative feedback to teams across the company

## Requirements
_Vary by job grade but include:_

- In-depth knowledge and an always up-to-date understanding of core social media platforms and how they are used to tell stories, sell products, and navigate crises
- Excellent writing and communication skills - both externally to our social audiences and internally across the company
- Extremely detail-oriented and organized
- Experience with rapid response strategies - for general engagement with the community and for navigating crises 
- Ability to learn to use GitLab as our project management and execution tool for all work (Don't worry, this comes with time. Be open to learning.)
- BONUS: A passion and strong understanding of the industry and our mission

### You must share our [values](/handbook/values/) and work under those values
_A sample of how GitLab's core values show up in Social Marketing:_
##### 🤝 Collaboration

Across teams and functions as well as externally with customers, followers, and partners. We are not islands to ourselves and lean on each other to deliver on our work. Do everything as kindly as possible. It's also critical to assume positive intent when working with team members that don't work in social media.

##### 📈 Results

Make a difference in the hours you planned to work and then do it again next week. Use GitLab to manage your work. Own everything you touch and create a bias for action in social media marketing.

##### ⏱️ Efficiency

Go for the boring solutions when we can't recreate the wheel. Search for the answers you seek in our handbook first. Build repeatable projects in a way that lets others service themselves in the next round.

##### 🌐 Diversity, Inclusion & Belonging

Be comfortable or willing to get comfortable with communication with team members asynchronously. Find a way to include your quirkiness in your work. Consider inclusive language and imagery in everything we promote internally and externally.

##### 👣 Iteration

Don't wait to contribute. Just make a change today. Make progress for our work in minimum viable changes consistently. Remember that all of our work is in draft. We don't do big reveals at GitLab, so be ready to share your rough drafts with everyone throughout the process (This is scary for social marketers, but our team is prepared to help you grow into this practice.)

##### 👁️ Transparency

Everything is public by default, including our typos, bad takes, and iterations. We strive to be transparent as often and as clearly as possible with each other and our community. Always share the _why_ and not just the what. We're practitioners and educators.

## Levels

### Social Marketing Associate

#### Job Grade
{:.no_toc}
The Social Marketing Associate is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

#### Responsibilities
{:.no_toc}
- Write copy and schedule organic social content across channels
- Monitor and engage with users across social channels who mention us, share their content with our team as appropriate
- Work with the team to ensure social amplification and engagement is incorporated as part of integrated campaign strategy and execution
- Contribute to reporting efforts across campaigns and milestones

#### Role Requirements
{:.no_toc}

- Minimum of 1 year of overall professional experience 
- 1-3 years of experience in a social media-related role
- Proven ability to write effective short-form content
- Able to coordinate with multiple stakeholders and perform in a fast-moving environment
- Proven ability to work on multiple projects at a time
- General experience with UTM tracking URLs, social media management tools like Sprout Social, and native social channels
- Openness towards learning about all areas of the business that could be connected to social media

------
### Social Marketing Manager

#### Job Grade 
{:.no_toc}
The Social Marketing Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

#### Responsibilities
{:.no_toc}
All of the responsibilities of an associate, plus:

- General management of the social calendar and craft social campaigns that will help engage, grow and educate our followers
- Contribute your perspective on what to publish, when, and where in support of campaigns
- Consult with interested team members on their personal social strategy and how they can help position themselves within the GitLab community
- Contribute to reporting on OKRs and other metrics

#### Requirements
{:.no_toc}
- Minimum of 3 years of overall professional experience 
- 3-5 years experience in a social marketing-related role, preferably in software marketing
- Proven ability to manage social media channels
- Proven ability to report on performance
- Experience managing social media communities

------
### Senior Social Marketing Manager

#### Job Grade 
{:.no_toc}
The Senior Social Marketing Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

#### Responsibilities
{:.no_toc}
All of the responsibilities of a manager, plus:

- Independently develop and execute social marketing campaigns from small to large
- Articulate & document social marketing strategy and process in the GitLab handbook
- Entrust work to other members of the social marketing team as appropriate
- Responsible for ideation of initiatives, OKRs, and reporting on results
- Staying up-to-date and educating others on current technologies and trends in social media
- Work with an in-house creative team to create engaging assets, sometimes designing quick needs yourself
- Provide insight into navigating communications risks and crises, sometimes executing on this alone

#### Requirements
{:.no_toc}
- Minimum of 5 years of overall professional experience 
- 3+ years in a social media-related role
- 1+ years of enterprise software marketing experience
- Proven experience managing B2B social media channels
- In-depth industry and technical community knowledge
- Proven ability to identify new audiences and channels, create a strategy, and execute the strategy
- Exposure to crisis communications and desire to learn more

------
### Manager, Social Marketing

Adding "Manager," to the front of the job title indicates that this role is a practice and people manager.

Managers in the Social Marketing team are prolific and creative social media strategists. They define GitLab’s social media marketing strategy and manage a world-class team to expand GitLab's influence, awareness, subscribers, and leads. They own the delivery and results of Social Marketing deliverables and OKRs. They are focused on improving results, productivity, and operational efficiency. They must also collaborate across departments to accomplish collective goals.

#### Job Grade 
{:.no_toc}
The Manager, Social Marketing is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

#### Responsibilities
{:.no_toc}
- Define, implement, and regularly iterate on GitLab's social media marketing strategy
- Oversee all social marketing initiatives
- Collaborate with various teams on campaign strategy, position, and messaging
- Perform regular gap analysis to identify areas of success, improvement, and opportunity
- Hire and manage a world-class team of social media managers and [follow GitLab's existing people manager responsibilities](https://about.gitlab.com/company/team/structure/#management-group) (take a peek now and know that you'll learn more about this when onboarding)

#### Requirements
{:.no_toc}
- Minimum of 7 years of overall professional experience 
- 5+ years in a social media-related role
- 1+ years of enterprise software marketing experience
- Experience defining the high-level strategy and creating social plans based on research and data
- Strong communication skills without fear of over-communication. This role will require effective collaboration and coordination across internal and external contributors
- Experience with crisis communications 
- BONUS: Experience in people management (we have an internal management development program)
- BONUS: A passion and strong understanding of the industry and our mission 

------
### Senior Manager, Social Marketing

Adding "Manager," to the front of the job title indicates that this role is a practice and people manager.

Senior Managers in the Social Marketing team take their role as a people manager and social media expert to the next level. They lead with data and guts. Senior Managers need to balance the "social" and the "media" of social media. They don't just define GitLab’s social media strategy, Senior Managers bring their personal philosophies and practices to teach everyone else on the team.

#### Job Grade 
{:.no_toc}
The Senior Manager, Social Marketing is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

#### Responsibilities
{:.no_toc}
- Integrate personal social media management philosophy, GitLab's brand voice, and GitLab's values into our social media strategy and content
- Lead on campaigns, positioning, and messaging as the expert on the GitLab brand
- Ability to roll up your sleeves when needed
- Grow and develop a world-class team of social media managers and [follow GitLab's existing people manager responsibilities](https://about.gitlab.com/company/team/structure/#management-group) (take a peek now and know that you'll learn more about this when onboarding)
- Act as a point-of-contact for all crises, incidents, and other emergencies 
- Ability to act discreetly with non-public information
- Lead as _the_ social media expert at GitLab for all brand and employee questions

#### Requirements
{:.no_toc}
- 8+ years of overall professional experience 
- 6+ years in a social media-related role
- 2+ years of enterprise software marketing experience
- 2+ years of people management experience
- Experience with crisis communications
- A defined point-of-view on the practice of social media marketing and communications

------
## Career Ladder

The next step in the Social Marketing job family is not yet defined at GitLab. 

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

Anonymous data on social media experience is not accepted. Candidates are required to submit information about the brands they've worked on during the application process. Please include names and data in your resume, cover letter, or other supplemental content to support your candidacy.

Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.

- Next, candidates will be invited to schedule a series of 30-45 minute interviews that may include: 
 - Our Head of Social Media
 - At least one other member of the social media team
 - Our Director of Corporate Communications 
 - A member of a supporting group in events, evangelism, partner marketing, or digital marketing programs
 - Our Vice President of Corporate Marketing
- Finally, our CMO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
